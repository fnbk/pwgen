# pwgen

A clone from https://github.com/anders/pwgen

# build
```
make
sudo make install
```

# usage
```
usage: ./pwgen [options]

Available options are:
  -a, --algorithm    Available algorithms: memorable, random, letters, alphanumeric, numbers.
                     The default is `memorable'.
  -c, --count        The number of passwords to generate (default: 5).
  -l, --length       Desired length of the generated passwords (default: 12).
  -L, --language     Generate passwords in a specified language.
                     Languages: en, de, es, fr, it, nl, pt, jp.
                     Note that this feature is broken and will produce garbage, bug: rdar://14889281
  -v, --version      Print the version number and exit.
  -h, --help         Print this message.
```

# z shell alias
```
alias password='pwgen --algorithm=memorable --count=1 --length=12'
```

# requirements
* Snow Leopard and later

